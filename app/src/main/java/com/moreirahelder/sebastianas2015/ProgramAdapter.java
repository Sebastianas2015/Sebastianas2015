package com.moreirahelder.sebastianas2015;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.ListIterator;

/**
 * Created by helder on 02-05-2015.
 */
public class ProgramAdapter extends RecyclerView.Adapter<ProgramAdapter.ProgramViewHolder> {

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;

    private Activity mainActivity;
    private ProgramItems program_list;
    private Context context;
    private SebsDB db;
    private ImagesHandler imagesHandler;
    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");

    public ProgramAdapter(Context context, Activity mainActivity) {
        this.context = context;
        this.mainActivity = mainActivity;
        this.db = SebsDB.getInstance(context);
        this.imagesHandler = ImagesHandler.getInstance(context);
        updateList();
    }

    public void updateList() {
        ArrayList<ProgramEvent> list = db.getProgram();
        new FillCache(list).execute();
        program_list = new ProgramItems(list);
    }

    private class FillCache extends AsyncTask {

        private ArrayList<ProgramEvent> list;

        public FillCache(ArrayList<ProgramEvent> list) {
            this.list = list;
        }

        @Override
        protected Object doInBackground(Object[] params) {
            ListIterator li = list.listIterator(list.size());
            while (li.hasPrevious()) {
                ProgramEvent item = (ProgramEvent) li.previous();
                item.setBitmap(imagesHandler.getImage(item.getUuid()));
            }
            return null;
        }
    }

    @Override
    public ProgramViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            View itemView = LayoutInflater.
                    from(parent.getContext()).
                    inflate(R.layout.program_cardview_layout, parent, false);
            return new ProgramViewHolder(itemView, viewType);
        } else {
            View itemView = LayoutInflater.
                    from(parent.getContext()).
                    inflate(R.layout.program_day_bar, parent, false);
            return new ProgramViewHolder(itemView, viewType);
        }


    }

    @Override
    public void onBindViewHolder(ProgramViewHolder holder, int position) {
        final ProgramEvent event = program_list.get(position);
        if (event != null) {
            holder.name.setText(event.getTitle());
            Bitmap bitmap = event.getBitmap();
            if (bitmap == null) {
                bitmap = ImagesHandler.getInstance(context).getImage(event.getUuid());
                BitmapsCache.getInstance().addBitmapToMemoryCache("" + event.getUuid(), bitmap);
            }
            if (bitmap != null) {
                holder.image.setImageBitmap(bitmap);
                holder.image.setAdjustViewBounds(true);
            } else {
                holder.image.setImageBitmap(null);
                holder.image.setAdjustViewBounds(false);
            }
            if (event.getDate().getHours() == 8 && event.getDate().getMinutes() == 0)
                holder.date.setText("");
            else
                holder.date.setText(sdf.format(event.getDate()));
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new EventDetails(mainActivity, event).show();
                }
            });
        } else {
            Date date = program_list.getDate(position);
            SimpleDateFormat dayf = new SimpleDateFormat("dd");
            SimpleDateFormat weekDayf = new SimpleDateFormat("EEEE");
            SimpleDateFormat monthf = new SimpleDateFormat("MMMM");
            holder.day_date.setText(dayf.format(date));
            holder.month_date.setText(monthf.format(date));

            String weekDay = weekDayf.format(date);
            char first = Character.toUpperCase(weekDay.charAt(0));
            holder.day_more.setText(first + weekDay.substring(1));
        }

    }

    @Override
    public int getItemCount() {
        return program_list.size();
    }


    @Override
    public int getItemViewType(int position) {
        if (program_list.isHeader(position))
            return TYPE_HEADER;
        else
            return TYPE_ITEM;
    }

    public static class ProgramViewHolder extends RecyclerView.ViewHolder {
        protected TextView name;
        protected ImageView image;
        protected TextView date;
        protected TextView day_date;
        protected TextView month_date;
        protected TextView day_more;

        public ProgramViewHolder(View v, int viewType) {
            super(v);
            if (viewType == TYPE_ITEM) {
                name = (TextView) v.findViewById(R.id.title);
                image = (ImageView) v.findViewById(R.id.image);
                date = (TextView) v.findViewById(R.id.date);
            } else {
                day_date = (TextView) v.findViewById(R.id.dayText);
                month_date = (TextView) v.findViewById(R.id.monthText);
                day_more = (TextView) v.findViewById(R.id.dayMore);
                StaggeredGridLayoutManager.LayoutParams layoutParams = ((StaggeredGridLayoutManager.LayoutParams) this.itemView.getLayoutParams());
                layoutParams.setFullSpan(true);
            }

        }
    }
}
