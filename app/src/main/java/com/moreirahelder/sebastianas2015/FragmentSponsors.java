package com.moreirahelder.sebastianas2015;

import android.app.Fragment;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

/**
 * Created by helder on 11-04-2015.
 */
public class FragmentSponsors extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    SwipeRefreshLayout swipeLayout;
    SponsorsAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().getActionBar().setTitle(getString(R.string.section_sponsors));

        View view = inflater.inflate(R.layout.fragment_sponsors, container, false);
        swipeLayout = (SwipeRefreshLayout) view.findViewById(R.id.sponsors_container);
        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setColorSchemeResources(R.color.blue, R.color.green, R.color.orange);

        RecyclerView recList = (RecyclerView) view.findViewById(R.id.cardList);
        recList.setHasFixedSize(true);
        final StaggeredGridLayoutManager layout_manager = new StaggeredGridLayoutManager(getNcolumns(), StaggeredGridLayoutManager.VERTICAL);
        recList.setLayoutManager(layout_manager);
        recList.setItemAnimator(new DefaultItemAnimator());
        layout_manager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_MOVE_ITEMS_BETWEEN_SPANS);
        adapter = new SponsorsAdapter(getActivity().getBaseContext());
        recList.setAdapter(adapter);
        recList.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int[] into = null;
                swipeLayout.setEnabled(layout_manager.findFirstCompletelyVisibleItemPositions(into)[0] == 0);
            }
        });

        return view;
    }

    @Override public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                new UpdateInBackground().execute();
            }
        }, 1000);
    }

    private class UpdateInBackground extends AsyncTask {

        public UpdateInBackground() {

        }

        @Override
        protected Object doInBackground(Object[] params) {
            new DownloadContents(getActivity().getBaseContext(), false, new RefreshCallback() {
                @Override
                public void callbackCall() {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            swipeLayout.setRefreshing(false);
                            refresh();
                        }
                    });
                }

                @Override
                public void sayNoInternet() {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getActivity(), getActivity().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    });

                }
            }).updateSponsors();
            return null;
        }
    }

    public void refresh(){
        adapter.updateList();
        adapter.notifyDataSetChanged();
    }

    public int getNcolumns() {
        int n = 1;

        if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE) {
            n++;
        }
        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            n++;
        }

        return n;
    }
}
