package com.moreirahelder.sebastianas2015;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by helder on 09-04-2015.
 */
public class DrawerAdapter extends RecyclerView.Adapter<DrawerAdapter.ViewHolder> {

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private static final int TYPE_FOOTER = 2;

    private boolean isFirstStart;
    private FragmentSelectCallback callback;
    private MenuOptions menu_opts;
    private int footer_position;
    private int index;

    Context context;

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        int Holderid;

        TextView textView;
        ImageView imageView;

        public ViewHolder(View itemView, int ViewType) {
            super(itemView);

            if (ViewType == TYPE_ITEM) {
                textView = (TextView) itemView.findViewById(R.id.rowText);
                imageView = (ImageView) itemView.findViewById(R.id.rowIcon);
                Holderid = 1;
                itemView.setClickable(true);
                itemView.setOnClickListener(this);
            } else if (ViewType == TYPE_FOOTER) {
                Holderid = 2;
            } else {
                Holderid = 0;
            }
        }

        @Override
        public void onClick(View v) {
            callback.selectFragment(menu_opts.getViewIndex(v));
        }
    }

    public DrawerAdapter(Context context, MenuOptions menu_opts, FragmentSelectCallback callback, boolean isFirstStart) {
        this.menu_opts = menu_opts;
        footer_position = menu_opts.size() + 1;
        this.context = context;
        this.callback = callback;
        this.isFirstStart = isFirstStart;
        this.index = 0;
    }

    @Override
    public DrawerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, final int viewType) {

        if (viewType == TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row, parent, false);
            ViewHolder vhItem = new ViewHolder(v, viewType);
            menu_opts.setView(index, vhItem.itemView);
            if ((!isFirstStart) && (index == menu_opts.getCurrentIndex())) {
                callback.selectFragment(index);
            }
            index++;
            return vhItem;

        } else if (viewType == TYPE_HEADER) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.header, parent, false);
            ViewHolder vhHeader = new ViewHolder(v, viewType);
            return vhHeader;

        } else if (viewType == TYPE_FOOTER) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.footer, parent, false);
            ImageView fb = (ImageView) v.findViewById(R.id.fb_img);
            fb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.addCategory(Intent.CATEGORY_BROWSABLE);
                    intent.setData(Uri.parse("http://www.fibramunde.pt/"));
                    context.startActivity(intent);
                }
            });
            ImageView asvs = (ImageView) v.findViewById(R.id.asvs_img);
            asvs.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.addCategory(Intent.CATEGORY_BROWSABLE);
                    intent.setData(Uri.parse("http://www.asvs.pt/pt/"));
                    context.startActivity(intent);
                }
            });
            ViewHolder vhHeader = new ViewHolder(v, viewType);
            return vhHeader;

        }
        return null;

    }

    @Override
    public void onBindViewHolder(DrawerAdapter.ViewHolder holder, int position) {
        if (holder.Holderid == 1) {
            holder.textView.setText(menu_opts.getTitle(position - 1));
            holder.imageView.setImageResource(menu_opts.getIcon(position - 1));
        }
    }

    @Override
    public int getItemCount() {
        return menu_opts.size() + 2;
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position))
            return TYPE_HEADER;
        if (isPositionFooter(position))
            return TYPE_FOOTER;
        return TYPE_ITEM;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    private boolean isPositionFooter(int position) {
        return position == footer_position;
    }

}