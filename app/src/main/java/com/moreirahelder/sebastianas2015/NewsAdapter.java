package com.moreirahelder.sebastianas2015;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.ListIterator;

/**
 * Created by helder on 02-05-2015.
 */
public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.NewViewHolder> {


    private Activity mainActivity;
    private ArrayList<NewsEvent> news_list;
    private Context context;
    private SebsDB db;
    private ImagesHandler imagesHandler;
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");

    public NewsAdapter(Context context, Activity mainActivity) {
        this.context = context;
        this.mainActivity = mainActivity;
        this.db = SebsDB.getInstance(context);
        this.imagesHandler = ImagesHandler.getInstance(context);
        updateList();
    }

    public void updateList() {
        news_list = db.getNews();
        new FillCache(news_list).execute();
    }

    private class FillCache extends AsyncTask {

        private ArrayList<NewsEvent> list;

        public FillCache(ArrayList<NewsEvent> list) {
            this.list = list;
        }

        @Override
        protected Object doInBackground(Object[] params) {
            ListIterator li = list.listIterator(list.size());
            while (li.hasPrevious()) {
                NewsEvent item = (NewsEvent) li.previous();
                item.setBitmap(imagesHandler.getImage(item.getUuid()));
            }
            return null;
        }
    }

    @Override
    public NewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.news_cardview_layout, parent, false);
        return new NewViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(NewViewHolder holder, int position) {
        final NewsEvent event = news_list.get(position);
        holder.name.setText(event.getTitle());
        Bitmap bitmap = event.getBitmap();
        if (bitmap == null){
            bitmap = ImagesHandler.getInstance(context).getImage(event.getUuid());
            BitmapsCache.getInstance().addBitmapToMemoryCache(""+event.getUuid(), bitmap);
        }
        if (bitmap != null){
            holder.image.setImageBitmap(bitmap);
            holder.image.setAdjustViewBounds(true);
        }
        else {
            holder.image.setImageBitmap(null);
            holder.image.setAdjustViewBounds(false);
        }
        holder.date.setText(sdf.format(event.getDate()));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new EventDetails(mainActivity, event).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return news_list.size();
    }

    public static class NewViewHolder extends RecyclerView.ViewHolder {
        protected TextView name;
        protected ImageView image;
        protected TextView date;

        public NewViewHolder(View v) {
            super(v);
            name = (TextView) v.findViewById(R.id.title);
            image = (ImageView) v.findViewById(R.id.image);
            date = (TextView) v.findViewById(R.id.date);
        }
    }
}
