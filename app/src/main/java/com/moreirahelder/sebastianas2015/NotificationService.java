package com.moreirahelder.sebastianas2015;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.preference.PreferenceManager;

public class NotificationService extends Service {

    public static final int PERIOD = 5 * 60 * 1000;
    boolean notify;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        notify = PreferenceManager.getDefaultSharedPreferences(this).getBoolean("notifications", true);
        if (notify) {
            DownloadContents download = new DownloadContents(getApplicationContext(), true, null);
            download.buildNotifications();
        }
        stopSelf();

        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        AlarmManager alarm = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarm.set(
                AlarmManager.RTC_WAKEUP,
                System.currentTimeMillis() + (PERIOD),
                PendingIntent.getService(this, 0, new Intent(this, NotificationService.class), 0)
        );
    }
}
