package com.moreirahelder.sebastianas2015;

import android.graphics.Bitmap;

/**
 * Created by helder on 30-04-2015.
 */
public class Sponsor implements  Comparable<Sponsor>{

    private long uuid;
    private String name, link, image;
    private int order;

    public Bitmap getBitmap() {
        return BitmapsCache.getInstance().getBitmapFromMemCache(""+uuid);
    }

    public void setBitmap(Bitmap bitmap) {
        BitmapsCache.getInstance().addBitmapToMemoryCache(""+uuid, bitmap);
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public long getUuid() {
        return uuid;
    }

    public String getName() {
        return name;
    }

    public String getLink() {
        return link;
    }

    public String getImage() {
        return image;
    }

    public void setUuid(long uuid) {
        this.uuid = uuid;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Sponsor() {

    }

    @Override
    public boolean equals(Object obj) {
        Sponsor event = (Sponsor) obj;
        return this.uuid == event.uuid;
    }

    @Override
    public int compareTo(Sponsor another) {
        return this.order - another.order;
    }
}
