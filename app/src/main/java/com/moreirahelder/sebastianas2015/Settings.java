package com.moreirahelder.sebastianas2015;


import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.view.MenuItem;

/**
 * Created by helder on 04-04-2015.
 */
public class Settings extends PreferenceActivity  {

    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
        ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle(getString(R.string.action_settings));

        Preference myPref = findPreference("update_interval");
        myPref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                stopService(new Intent(getApplicationContext(), DownloadService.class));
                startService(new Intent(getApplicationContext(), DownloadService.class));
                return true;
            }
        });

        myPref = findPreference("notifications");
        myPref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                stopService(new Intent(getApplicationContext(), DownloadService.class));
                startService(new Intent(getApplicationContext(), DownloadService.class));
                return true;
            }
        });

        myPref = findPreference("cleanDataBase");
        myPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                AlertDialog.Builder builder = new AlertDialog.Builder(Settings.this);

                builder.setTitle("Limpar Base de Dados");
                builder.setMessage("Tem a certeza que pretende limpar toda a base de dados?");

                builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        SebsDB.getInstance(getApplicationContext()).reset();
                        dialog.dismiss();
                    }

                });

                builder.setNegativeButton("Não", null);
                AlertDialog alert = builder.create();
                alert.show();
                return false;
            }
        });

        myPref = findPreference("version");
        myPref.setSummary(BuildConfig.VERSION_NAME);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                Intent homeIntent = new Intent(this, MainActivity.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
        }
        return (super.onOptionsItemSelected(menuItem));
    }
}
