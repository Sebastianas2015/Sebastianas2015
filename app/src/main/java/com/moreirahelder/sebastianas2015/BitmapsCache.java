package com.moreirahelder.sebastianas2015;

import android.util.LruCache;
import android.graphics.Bitmap;

/**
 * Created by helder on 25-05-2015.
 */
public class BitmapsCache {

    private static BitmapsCache instance = null;
    private LruCache<String, Bitmap> mMemoryCache;

    public static BitmapsCache getInstance() {
        if (instance == null)
            instance = new BitmapsCache();
        return instance;
    }

    private BitmapsCache() {

        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        final int cacheSize = maxMemory / 2;

        mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                return bitmap.getByteCount() / 1024;
            }
        };
    }

    public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
        if (getBitmapFromMemCache(key) == null && bitmap != null) {
            mMemoryCache.put(key, bitmap);
        }
    }

    public Bitmap getBitmapFromMemCache(String key) {
        return mMemoryCache.get(key);
    }

    public long size() {
        return mMemoryCache.size();
    }
}
