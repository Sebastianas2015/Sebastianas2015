package com.moreirahelder.sebastianas2015;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.net.InetAddress;

/**
 * Created by helder on 10-04-2015.
 */
public class FragmentStream extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().getActionBar().setTitle(getString(R.string.section_stream));
        View view = inflater.inflate(R.layout.fragment_stream, container, false);

        final SharedPreferences sp = getActivity().getSharedPreferences("stream_pref", 0);
        if (isOnline() && sp.getBoolean("streamOnline", false)){
            ImageView image = (ImageView) view.findViewById(R.id.image_youtube);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                image.setBackground(getResources().getDrawable(R.drawable.youtube_color));
            }
            else{
                image.setBackgroundDrawable(getResources().getDrawable(R.drawable.youtube_color));
            }
            image.setOnClickListener(new View.OnClickListener(){
                public void onClick(View v){
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(sp.getString("streamLink", "")));
                    startActivity(intent);
                }
            });
            TextView text = (TextView) view.findViewById(R.id.textView5);
            text.setText("Online");
        }

        return view;
    }

    private boolean isOnline() {
        ConnectivityManager connMgr = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }
}
