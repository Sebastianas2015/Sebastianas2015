package com.moreirahelder.sebastianas2015;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


public class MainActivity extends Activity implements FragmentSelectCallback{

    int ICONS[] = {R.drawable.ic_home,
            R.drawable.ic_news,
            R.drawable.ic_map,
            R.drawable.ic_stream,
            R.drawable.ic_findus,
            R.drawable.ic_sponsors};

    public static MenuOptions menu_opts = null;
    RecyclerView mRecyclerView;
    RecyclerView.Adapter mAdapter;
    RecyclerView.LayoutManager mLayoutManager;
    DrawerLayout Drawer;
    ActionBarDrawerToggle mDrawerToggle;
    FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences sp = this.getSharedPreferences("init_pref", 0);
        boolean isFirstStart = sp.getBoolean("firstStart", true);
        Bundle extras = getIntent().getExtras();

        boolean fromNotification = false;
        if (extras != null)
            fromNotification = extras.getBoolean("fromNotification");
        fragmentManager = getFragmentManager();

        ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        mRecyclerView = (RecyclerView) findViewById(R.id.RecyclerView);
        mRecyclerView.setHasFixedSize(true);

        Drawer = (DrawerLayout) findViewById(R.id.DrawerLayout);
        if (menu_opts == null)
            menu_opts = new MenuOptions(this, this.getResources().getStringArray(R.array.section_list), ICONS);
        if (fromNotification)
            menu_opts.setCurrentIndex(1);
        mAdapter = new DrawerAdapter(this, menu_opts, this, isFirstStart);

        mRecyclerView.setAdapter(mAdapter);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mDrawerToggle = new ActionBarDrawerToggle(this, Drawer, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }


        };
        Drawer.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        if (isFirstStart) {
            Drawer.openDrawer(Gravity.LEFT);
            SharedPreferences.Editor e = sp.edit();
            e.putBoolean("firstStart", false);
            e.apply();
        }

        RateItDialogFragment.show(this, getFragmentManager());

        startService(new Intent(getApplicationContext(), DownloadService.class));
        startService(new Intent(getApplicationContext(), NotificationService.class));
    }

    public void selectFragment(int index){
        Fragment frag;
        switch (index) {
            case 0:
                frag = new FragmentProgram();
                fragmentManager.beginTransaction().replace(R.id.main_container, frag).commit();
                Drawer.closeDrawers();
                break;
            case 1:
                frag = new FragmentNews();
                fragmentManager.beginTransaction().replace(R.id.main_container, frag).commit();
                Drawer.closeDrawers();
                break;
            case 2:
                frag = new FragmentMap();
                fragmentManager.beginTransaction().replace(R.id.main_container, frag).commit();
                Drawer.closeDrawers();
                break;
            case 3:
                frag = new FragmentStream();
                fragmentManager.beginTransaction().replace(R.id.main_container, frag).commit();
                Drawer.closeDrawers();
                break;
            case 4:
                frag = new FragmentFindUs();
                fragmentManager.beginTransaction().replace(R.id.main_container, frag).commit();
                Drawer.closeDrawers();
                break;
            case 5:
                frag = new FragmentSponsors();
                fragmentManager.beginTransaction().replace(R.id.main_container, frag).commit();
                Drawer.closeDrawers();
                break;
        }
        menu_opts.setCurrentIndex(index);
        menu_opts.selectCurrent();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            startActivity(new Intent(this, Settings.class));
            return true;
        }

        if (id == R.id.action_about) {
            final Dialog dialog = new Dialog(this);
            dialog.setContentView(R.layout.about);
            dialog.setTitle("Sobre a Aplicação");
            ((TextView)dialog.findViewById(android.R.id.title)).setGravity(Gravity.CENTER);

           /* Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
            dialogButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });*/

            ((TextView)dialog.findViewById(R.id.appversion)).setText("Versão: "+BuildConfig.VERSION_NAME);

            ImageView img = (ImageView) dialog.findViewById(R.id.image_plus);
            img.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.addCategory(Intent.CATEGORY_BROWSABLE);
                    intent.setData(Uri.parse(getString(R.string.developer_plus)));
                    startActivity(intent);
                }
            });

            img = (ImageView) dialog.findViewById(R.id.image_face);
            img.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.addCategory(Intent.CATEGORY_BROWSABLE);
                    intent.setData(Uri.parse(getString(R.string.developer_face)));
                    startActivity(intent);
                }
            });

            img = (ImageView) dialog.findViewById(R.id.image_website);
            img.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.addCategory(Intent.CATEGORY_BROWSABLE);
                    intent.setData(Uri.parse(getString(R.string.developer_website)));
                    startActivity(intent);
                }
            });

            img = (ImageView) dialog.findViewById(R.id.image_in);
            img.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.addCategory(Intent.CATEGORY_BROWSABLE);
                    intent.setData(Uri.parse(getString(R.string.developer_in)));
                    startActivity(intent);
                }
            });

            img = (ImageView) dialog.findViewById(R.id.image_email);
            img.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                            "mailto", getString(R.string.developer_mail), null));
                    emailIntent.putExtra(Intent.EXTRA_SUBJECT, "[APP]Sebastianas2015");
                    startActivity(Intent.createChooser(emailIntent, "Enviar email..."));
                    startActivity(emailIntent);
                }
            });

            dialog.show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}