package com.moreirahelder.sebastianas2015;

import android.content.Context;
import android.view.View;

/**
 * Created by helder on 04-05-2015.
 */
public class MenuOptions {

    private Context context;
    private String[] title;
    private int[] icon;
    private View[] view;
    private int lastIndex;
    private int current_selected;

    public MenuOptions(Context context, String[] title, int[] icon) {
        this.title = title;
        this.icon = icon;
        this.context = context;
        this.view = new View[title.length];
        this.lastIndex = -1;
        this.current_selected = 0;
    }

    public String getTitle(int index) {
        return title[index];
    }

    public int getIcon(int index) {
        return icon[index];
    }

    public void setView(int index, View view) {
        this.view[index] = view;
    }

    public int getViewIndex(View v) {
        for (int i = 0; i < view.length; i++) {
            if (view[i].equals(v)) {
                return i;
            }
        }
        return -1;
    }

    public void select(int index) {
        view[index].setBackgroundColor(context.getResources().getColor(R.color.ItemSelected));
    }

    public void setCurrentIndex(int index){
        current_selected = index;
    }

    public void setLastIndex(int index){
        this.lastIndex = index;
    }

    public int getCurrentIndex(){
        return current_selected;
    }

    public void reset(int index) {
        this.view[index].setBackgroundColor(context.getResources().getColor(R.color.White));
    }

    public int size() {
        return title.length;
    }

    public void selectCurrent() {
        if (lastIndex != -1)
            reset(lastIndex);
        lastIndex = current_selected;
        select(current_selected);
    }

}
