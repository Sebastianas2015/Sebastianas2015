package com.moreirahelder.sebastianas2015;

import android.graphics.Bitmap;

import java.util.Date;

/**
 * Created by helder on 30-04-2015.
 */
public class NewsEvent implements  Comparable<NewsEvent>{

    private long uuid;
    private String title, desc, image, video;
    private Date date;

    public Bitmap getBitmap() {
        return BitmapsCache.getInstance().getBitmapFromMemCache(""+uuid);
    }

    public void setBitmap(Bitmap bitmap) {
        BitmapsCache.getInstance().addBitmapToMemoryCache(""+uuid, bitmap);
    }

    public int getNotify() {
        return notify;
    }

    public void setNotify(int notify) {
        this.notify = notify;
    }

    private int notify;

    public long getUuid() {
        return uuid;
    }

    public String getTitle() {
        return title;
    }

    public String getDesc() {
        return desc;
    }

    public String getImage() {
        return image;
    }

    public String getVideo() {
        return video;
    }

    public Date getDate() {
        return date;
    }

    public void setUuid(long uuid) {
        this.uuid = uuid;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public  NewsEvent(){

    }

    @Override
    public boolean equals(Object obj) {
        NewsEvent event = (NewsEvent) obj;
        return this.uuid == event.uuid;
    }

    @Override
    public int compareTo(NewsEvent another) {
        return this.date.compareTo(another.date);
    }
}
