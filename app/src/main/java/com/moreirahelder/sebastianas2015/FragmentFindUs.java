package com.moreirahelder.sebastianas2015;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

/**
 * Created by helder on 10-04-2015.
 */
public class FragmentFindUs extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().getActionBar().setTitle(getString(R.string.section_findus));
        return inflater.inflate(R.layout.fragment_findus, container, false);
    }

    @Override
    public void onViewCreated(View v, Bundle savedInstanceState) {
        ImageView img = (ImageView) getActivity().findViewById(R.id.image_website);
        img.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse(getString(R.string.find_website)));
                startActivity(intent);
            }
        });

        img = (ImageView) getActivity().findViewById(R.id.image_email);
        img.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto", getString(R.string.find_email), null));
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "[APP] Sebastianas2015");
                startActivity(Intent.createChooser(emailIntent, "Enviar email..."));
                startActivity(emailIntent);
            }
        });

        img = (ImageView) getActivity().findViewById(R.id.image_plus);
        img.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse(getString(R.string.find_plus)));
                startActivity(intent);
            }
        });

        img = (ImageView) getActivity().findViewById(R.id.image_face);
        img.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse(getString(R.string.find_facebook)));
                startActivity(intent);
            }
        });

        img = (ImageView) getActivity().findViewById(R.id.image_twitter);
        img.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse(getString(R.string.find_twitter)));
                startActivity(intent);
            }
        });

        img = (ImageView) getActivity().findViewById(R.id.image_instagram);
        img.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse(getString(R.string.find_instagram)));
                startActivity(intent);
            }
        });

        img = (ImageView) getActivity().findViewById(R.id.image_youtube);
        img.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse(getString(R.string.find_youtube)));
                startActivity(intent);
            }
        });

        img = (ImageView) getActivity().findViewById(R.id.image_maps);
        img.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse(getString(R.string.find_map)));
                startActivity(intent);
            }
        });
    }
}
