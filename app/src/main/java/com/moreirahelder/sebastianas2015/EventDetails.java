package com.moreirahelder.sebastianas2015;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.view.Gravity;
import android.view.View;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;

/**
 * Created by helder on 08-05-2015.
 */
public class EventDetails extends Dialog {

    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
    SimpleDateFormat day_only = new SimpleDateFormat("dd-MM-yyyy");

    private ImagesHandler img_handler;
    private Context context;
    private ProgramEvent p_event;
    private NewsEvent n_event;
    private boolean isProgram;

    private TextView title;
    private TextView date;
    private TextView description;
    private ImageView image;
    private ImageView video;

    public EventDetails(final Context context, Object event) {
        super(context);
        this.context = context;


        img_handler = ImagesHandler.getInstance(context);
        if (event instanceof ProgramEvent) {
            isProgram = true;
            this.setContentView(R.layout.program_popup);
            p_event = (ProgramEvent) event;
        } else {
            isProgram = false;
            this.setContentView(R.layout.news_popup);
            n_event = (NewsEvent) event;
        }

        title = (TextView) this.findViewById(android.R.id.title);
        title.setGravity(Gravity.CENTER);
        title.setTextSize(25);

        //title = (TextView) this.findViewById(R.id.p_title);
        date = (TextView) this.findViewById(R.id.p_date);
        description = (TextView) this.findViewById(R.id.p_description);
        image = (ImageView) this.findViewById(R.id.p_image);
        video = (ImageView) this.findViewById(R.id.video);
        setValues();
        getWindow().setLayout(GridLayout.LayoutParams.FILL_PARENT, GridLayout.LayoutParams.FILL_PARENT);
    }

    private void setValues() {
        if (isProgram) {
            setTitle(p_event.getTitle());
            if (p_event.getDate().getHours() == 8 && p_event.getDate().getMinutes() == 0)
                date.setText(day_only.format(p_event.getDate()));
            else
                date.setText(sdf.format(p_event.getDate()));
            description.setText(p_event.getDesc());
            Bitmap bitmap = img_handler.getImage(p_event.getUuid());
            if (bitmap != null) {
                image.setImageBitmap(bitmap);
            } else {
                image.setAdjustViewBounds(false);
            }
            final String video_link = p_event.getVideo();
            if (!video_link.equals("")) {
                video.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        Intent intent = new Intent();
                        intent.setAction(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse(video_link));
                        context.startActivity(intent);
                    }
                });
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    video.setBackground(context.getResources().getDrawable(R.drawable.youtube_bw));
                } else {
                    video.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.youtube_bw));
                }
            }

        } else {
            setTitle(n_event.getTitle());
            date.setText(sdf.format(n_event.getDate()));
            description.setText(n_event.getDesc());
            Bitmap bitmap = img_handler.getImage(n_event.getUuid());
            if (bitmap != null) {
                image.setImageBitmap(bitmap);
            } else {
                image.setAdjustViewBounds(false);
            }
            final String video_link = n_event.getVideo();
            if (!video_link.equals("")) {
                video.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        Intent intent = new Intent();
                        intent.setAction(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse(video_link));
                        context.startActivity(intent);
                    }
                });
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    video.setBackground(context.getResources().getDrawable(R.drawable.youtube_bw));
                } else {
                    video.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.youtube_bw));
                }
            }
        }
    }
}
