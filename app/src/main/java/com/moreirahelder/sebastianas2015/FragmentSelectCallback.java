package com.moreirahelder.sebastianas2015;

/**
 * Created by helder on 05-05-2015.
 */
public interface FragmentSelectCallback {
    void selectFragment(int index);
}
