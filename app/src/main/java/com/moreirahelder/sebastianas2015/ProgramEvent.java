package com.moreirahelder.sebastianas2015;

import android.graphics.Bitmap;

import java.util.Date;

/**
 * Created by helder on 30-04-2015.
 */
public class ProgramEvent implements Comparable<ProgramEvent>{

    private long uuid;
    private String title, desc, image, video;
    private Date date;

    public Bitmap getBitmap() {
        return BitmapsCache.getInstance().getBitmapFromMemCache(""+uuid);
    }

    public void setBitmap(Bitmap bitmap) {
        BitmapsCache.getInstance().addBitmapToMemoryCache(""+uuid, bitmap);
    }

    public long getUuid() {
        return uuid;
    }

    public String getTitle() {
        return title;
    }

    public String getDesc() {
        return desc;
    }

    public String getImage() {
        return image;
    }

    public String getVideo() {
        return video;
    }

    public Date getDate() {
        return date;
    }

    public void setUuid(long uuid) {
        this.uuid = uuid;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public ProgramEvent(){

    }

    @Override
    public boolean equals(Object obj) {
        ProgramEvent event = (ProgramEvent) obj;
        return this.uuid == event.uuid;
    }

    @Override
    public int compareTo(ProgramEvent another) {
        Date date1 = (Date) this.date.clone();
        if (date1.getHours() <= 8){
            date1.setHours(date1.getHours() + 24);
        }
        Date date2 = (Date) another.date.clone();
        if (date2.getHours() <= 8){
            date2.setHours(date2.getHours() + 24);
        }
        return date1.compareTo(date2);
    }
}
