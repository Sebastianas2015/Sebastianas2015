package com.moreirahelder.sebastianas2015;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by helder on 02-05-2015.
 */
public class SponsorsAdapter extends RecyclerView.Adapter<SponsorsAdapter.SponsorViewHolder> {

    private ArrayList<Sponsor> sponsor_list;
    private Context context;
    private SebsDB db;
    private ImagesHandler imagesHandler;

    public SponsorsAdapter(Context context) {
        this.context = context;
        this.db = SebsDB.getInstance(context);
        this.imagesHandler = ImagesHandler.getInstance(context);
        updateList();
    }

    public void updateList() {
        sponsor_list = db.getSponsors();
        new FillCache(sponsor_list).execute();
    }

    private class FillCache extends AsyncTask {

        private ArrayList<Sponsor> list;

        public FillCache(ArrayList<Sponsor> list) {
            this.list = list;
        }

        @Override
        protected Object doInBackground(Object[] params) {
            ListIterator li = list.listIterator(list.size());
            while (li.hasPrevious()) {
                Sponsor item = (Sponsor) li.previous();
                item.setBitmap(imagesHandler.getImage(item.getUuid()));
            }
            return null;
        }
    }

    @Override
    public SponsorViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.sponsors_cardview_layout, parent, false);
        return new SponsorViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SponsorViewHolder holder, int position) {
        final Sponsor sponsor = sponsor_list.get(position);
        holder.name.setText(sponsor.getName());
        Bitmap bitmap = sponsor.getBitmap();
        if (bitmap == null){
            bitmap = ImagesHandler.getInstance(context).getImage(sponsor.getUuid());
            BitmapsCache.getInstance().addBitmapToMemoryCache(""+sponsor.getUuid(), bitmap);
        }
        if (bitmap != null){
            holder.image.setImageBitmap(bitmap);
            holder.image.setAdjustViewBounds(true);
        }
        else {
            holder.image.setImageBitmap(null);
            holder.image.setAdjustViewBounds(false);
        }
        if (!sponsor.getLink().equals(""))
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(sponsor.getLink()));
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            });
    }

    @Override
    public int getItemCount() {
        return sponsor_list.size();
    }

    public static class SponsorViewHolder extends RecyclerView.ViewHolder {
        protected TextView name;
        protected ImageView image;

        public SponsorViewHolder(View v) {
            super(v);
            name = (TextView) v.findViewById(R.id.name);
            image = (ImageView) v.findViewById(R.id.image);
        }
    }
}
