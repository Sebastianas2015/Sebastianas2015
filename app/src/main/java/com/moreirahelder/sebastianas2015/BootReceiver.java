package com.moreirahelder.sebastianas2015;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by helder on 09-05-2015.
 */
public class BootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        context.startService(new Intent(context, DownloadService.class));
        context.startService(new Intent(context, NotificationService.class));
    }
}
