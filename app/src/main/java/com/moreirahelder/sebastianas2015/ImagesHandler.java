package com.moreirahelder.sebastianas2015;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/**
 * Created by helder on 01-05-2015.
 */
public class ImagesHandler {

    private static ImagesHandler instance = null;

    public static ImagesHandler getInstance(Context context){
        if (instance == null)
            instance = new ImagesHandler(context);
        return instance;
    }

    Context context;

    private ImagesHandler(Context context) {
        this.context = context;
    }

    public void saveImage(String url, long uuid) {
        new DownloadImageTask(uuid).execute(url, "" + uuid);
    }

    private void writeToFile(Bitmap bitmap, String filename) {
        FileOutputStream out = null;
        try {
            out = context.openFileOutput(filename, Context.MODE_PRIVATE);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean exists(long uuid) {
        File file = context.getFileStreamPath("image" + uuid + ".png");
        return !(file == null || !file.exists());
    }

    public Bitmap getImage(long uuid) {
        FileInputStream fis;
        try {
            fis = context.openFileInput("image" + uuid + ".png");
        } catch (FileNotFoundException e) {
            return null;
        }
        if (fis == null)
            return null;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Bitmap bitmap = BitmapFactory.decodeStream(fis, null, options);
        try {
            fis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;
    }


    public void deleteImage(long uuid) {
        context.deleteFile("image" + uuid + ".png");
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {

        private String filename;
        public DownloadImageTask(Long uuid) {
            filename = "image" + uuid + ".png";
        }

        @Override
        protected Bitmap doInBackground(String... urls) {
            Bitmap bitmap = null;
            HttpGet httpRequest = new HttpGet(urls[0]);
            HttpClient httpclient = new DefaultHttpClient();
            HttpResponse response = null;
            try {
                response = (HttpResponse) httpclient.execute(httpRequest);
                HttpEntity entity = response.getEntity();
                BufferedHttpEntity bufferedHttpEntity = new BufferedHttpEntity(entity);
                InputStream in = bufferedHttpEntity.getContent();
                bitmap = BitmapFactory.decodeStream(in);
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            if (result != null)
                writeToFile(result, filename);
        }
    }
}
