package com.moreirahelder.sebastianas2015;

import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by helder on 10-04-2015.
 */
public class FragmentMap extends Fragment{

    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().getActionBar().setTitle(getString(R.string.section_map));
        view = inflater.inflate(R.layout.fragment_map, container, false);

        TouchImageView image = (TouchImageView) view.findViewById(R.id.map);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;
        Bitmap bitmap = BitmapFactory.decodeResource(getActivity().getResources(),R.drawable.sbtmap, options);
        if (bitmap != null)
            image.setImageBitmap(bitmap);

        ImageView info = (ImageView) view.findViewById(R.id.info);
        info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getActivity().getFragmentManager();
                InfoDialog.newInstance().show(fm,"dialog");
            }
        });
        return view;
    }

    public static class InfoDialog extends DialogFragment {

        private static InfoDialog dialog = null;

        static InfoDialog newInstance() {
            if (dialog==null)
                dialog = new InfoDialog();
            return dialog;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            int style = DialogFragment.STYLE_NO_TITLE, theme = 0;
            setStyle(style, theme);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View v = inflater.inflate(R.layout.dialog_desc, container, false);
            return v;
        }
    }
}
