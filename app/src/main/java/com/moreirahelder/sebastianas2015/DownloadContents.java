package com.moreirahelder.sebastianas2015;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Created by helder on 01-05-2015.
 */
public class DownloadContents {

    String urlString = "http://www.sebastianas.com/sbt_ext/";
    SebsDB db;
    ImagesHandler image_handler;

    boolean backgroundService;
    Context context;

    URL program_url;
    URL news_url;
    URL sponsors_url;
    URL main_url;

    RefreshCallback callback;

    public DownloadContents(Context context, boolean backgroundService, RefreshCallback callback) {
        try {
            this.context = context;
            this.backgroundService = backgroundService;
            this.callback = callback;
            db = SebsDB.getInstance(context);
            image_handler = ImagesHandler.getInstance(context);
            program_url = new URL(urlString + "program.xml");
            news_url = new URL(urlString + "news.xml");
            sponsors_url = new URL(urlString + "sponsors.xml");
            main_url = new URL(urlString + "main.xml");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public static boolean isMobileConnected(Context context) {
        ConnectivityManager connManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        return ((netInfo != null) && netInfo.isConnected());
    }

    public void updateMainSettings() {
        if (!isOnline()) {
            if (!backgroundService) {
                if (callback != null) {
                    callback.sayNoInternet();
                    callback.callbackCall();
                }
            }
            return;
        }
        new DownloadMainXml().execute(main_url);
    }

    public void saveMain(String document) {

        Document doc = getDomElement(document);
        if (doc == null)
            return;
        Element settings = doc.getDocumentElement();

        SharedPreferences sp = context.getSharedPreferences("stream_pref", 0);
        SharedPreferences.Editor e = sp.edit();
        if (getValue(settings, "streamOnline").equals("true")) {
            e.putBoolean("streamOnline", true);
            e.putString("streamLink", getValue(settings, "streamLink"));
        } else {
            e.putBoolean("streamOnline", false);
            e.putString("streamLink", getValue(settings, ""));
        }
        e.apply();
    }

    public void updateProgram() {
        if (!isOnline()) {
            if (!backgroundService) {
                if (callback != null) {
                    callback.sayNoInternet();
                    callback.callbackCall();
                }
            }
            return;
        }
        new DownloadProgramXml().execute(program_url);
    }

    public void saveProgram(String document) {
        Document doc = getDomElement(document);
        if (doc == null)
            return;
        Element root = doc.getDocumentElement();

        NodeList nl = root.getElementsByTagName("event");
        for (int i = 0; i < nl.getLength(); i++) {
            Element item = (Element) nl.item(i);
            NodeList elems = item.getElementsByTagName("*");
            long uuid = 0;
            String title = "", desc = "", image = "", video = "";
            int day = -1, month = -1, hour = -1, minutes = -1;
            for (int j = 0; j < elems.getLength(); j++) {
                Element e = (Element) elems.item(j);
                switch (e.getTagName()) {
                    case "uuid":
                        uuid = Long.parseLong(getElementValue(e));
                        break;
                    case "title":
                        title = getElementValue(e);
                        break;
                    case "description":
                        desc = getElementValue(e);
                        break;
                    case "image":
                        boolean images_wifi_only = PreferenceManager.getDefaultSharedPreferences(context).getBoolean("images_wifionly", false);
                        if (images_wifi_only && isMobileConnected(context)) {
                            break;
                        }
                        image = getElementValue(e);
                        if ((!db.ProgramSameImage(uuid, image)) || (!image_handler.exists(uuid)))
                            image_handler.saveImage(image, uuid);
                        break;
                    case "video":
                        video = getElementValue(e);
                        break;
                    case "day":
                        day = Integer.parseInt(getElementValue(e));
                        break;
                    case "month":
                        month = Integer.parseInt(getElementValue(e));
                        break;
                    case "hour":
                        hour = Integer.parseInt(getElementValue(e));
                        break;
                    case "minutes":
                        minutes = Integer.parseInt(getElementValue(e));
                        break;
                }
            }
            db.insertProgramEvent(uuid, title, desc, image, video, day, month, hour, minutes);
        }

        ArrayList<Long> list = new ArrayList<>();
        nl = root.getElementsByTagName("deleted_events");
        for (int i = 0; i < nl.getLength(); i++) {
            Element item = (Element) nl.item(i);
            NodeList elems = item.getElementsByTagName("*");
            long uuid = 0;
            for (int j = 0; j < elems.getLength(); j++) {
                Element e = (Element) elems.item(j);
                uuid = Long.parseLong(getElementValue(e));
                list.add(uuid);
            }
        }

        db.clearProgram(list);
        if (callback != null)
            callback.callbackCall();
    }

    public void updateNews() {
        if (!isOnline()) {
            if (!backgroundService) {
                if (callback != null) {
                    callback.sayNoInternet();
                    callback.callbackCall();
                }
            }
            return;
        }
        new DownloadNewsXml().execute(news_url);
    }

    public void saveNews(String document) {
        Document doc = getDomElement(document);
        if (doc == null)
            return;
        Element root = doc.getDocumentElement();
        SharedPreferences sp = context.getSharedPreferences("init_notify", 0);
        boolean firstTimeNotify = sp.getBoolean("firstTimeNotify", true);

        NodeList nl = root.getElementsByTagName("event");
        for (int i = 0; i < nl.getLength(); i++) {
            Element item = (Element) nl.item(i);
            NodeList elems = item.getElementsByTagName("*");
            long uuid = 0;
            String title = "", desc = "", image = "", video = "";
            int day = -1, month = -1, hour = -1, minutes = -1, notify = 0;
            for (int j = 0; j < elems.getLength(); j++) {
                Element e = (Element) elems.item(j);
                switch (e.getTagName()) {
                    case "uuid":
                        uuid = Long.parseLong(getElementValue(e));
                        break;
                    case "title":
                        title = getElementValue(e);
                        break;
                    case "description":
                        desc = getElementValue(e);
                        break;
                    case "image":
                        boolean images_wifi_only = PreferenceManager.getDefaultSharedPreferences(context).getBoolean("images_wifionly", false);
                        if (images_wifi_only && isMobileConnected(context)) {
                            break;
                        }
                        image = getElementValue(e);
                        if ((!db.NewsSameImage(uuid, image)) || (!image_handler.exists(uuid)))
                            image_handler.saveImage(image, uuid);
                        break;
                    case "video":
                        video = getElementValue(e);
                        break;
                    case "day":
                        day = Integer.parseInt(getElementValue(e));
                        break;
                    case "month":
                        month = Integer.parseInt(getElementValue(e));
                        break;
                    case "hour":
                        hour = Integer.parseInt(getElementValue(e));
                        break;
                    case "minutes":
                        minutes = Integer.parseInt(getElementValue(e));
                        break;
                    case "notify":
                        if (getElementValue(e).equals("true")) {
                            if (!firstTimeNotify) {
                                notify = 1;
                            } else {
                                Calendar current = Calendar.getInstance();
                                GregorianCalendar calendar = new GregorianCalendar(current.get(Calendar.YEAR), month - 1, day, hour, minutes);
                                if (calendar.getTime().getTime() >= new Date().getTime()) {
                                    notify = 1;
                                }
                            }
                        }
                        break;
                }
            }
            db.insertNewsEvent(uuid, title, desc, image, video, day, month, hour, minutes, notify);
        }

        if (firstTimeNotify) {
            SharedPreferences.Editor e = sp.edit();
            e.putBoolean("firstTimeNotify", false);
            e.apply();
        }

        ArrayList<Long> list = new ArrayList<>();
        nl = root.getElementsByTagName("deleted_events");
        for (int i = 0; i < nl.getLength(); i++) {
            Element item = (Element) nl.item(i);
            NodeList elems = item.getElementsByTagName("*");
            long uuid = 0;
            for (int j = 0; j < elems.getLength(); j++) {
                Element e = (Element) elems.item(j);
                uuid = Long.parseLong(getElementValue(e));
                list.add(uuid);
            }
        }

        db.clearNews(list);
        if (callback != null)
            callback.callbackCall();
    }

    public void updateSponsors() {
        if (!isOnline()) {
            if (!backgroundService) {
                if (callback != null) {
                    callback.sayNoInternet();
                    callback.callbackCall();
                }
            }
            return;
        }
        new DownloadSponsorsXml().execute(sponsors_url);
    }

    public void saveSponsors(String document) {
        Document doc = getDomElement(document);
        if (doc == null)
            return;
        Element root = doc.getDocumentElement();

        NodeList nl = root.getElementsByTagName("event");
        for (int i = 0; i < nl.getLength(); i++) {
            Element item = (Element) nl.item(i);
            NodeList elems = item.getElementsByTagName("*");
            long uuid = 0;
            String title = "", link = "", image = "";
            for (int j = 0; j < elems.getLength(); j++) {
                Element e = (Element) elems.item(j);
                switch (e.getTagName()) {
                    case "uuid":
                        uuid = Long.parseLong(getElementValue(e));
                        break;
                    case "title":
                        title = getElementValue(e);
                        break;
                    case "link":
                        link = getElementValue(e);
                        break;
                    case "image":
                        boolean images_wifi_only = PreferenceManager.getDefaultSharedPreferences(context).getBoolean("images_wifionly", false);
                        if (images_wifi_only && isMobileConnected(context)) {
                            break;
                        }
                        image = getElementValue(e);
                        if ((!db.SponsorSameImage(uuid, image)) || (!image_handler.exists(uuid)))
                            image_handler.saveImage(image, uuid);
                        break;
                }
            }
            db.insertSponsor(uuid, title, image, link, i);
        }

        ArrayList<Long> list = new ArrayList<>();
        nl = root.getElementsByTagName("deleted_sponsors");
        for (int i = 0; i < nl.getLength(); i++) {
            Element item = (Element) nl.item(i);
            NodeList elems = item.getElementsByTagName("*");
            long uuid = 0;
            for (int j = 0; j < elems.getLength(); j++) {
                Element e = (Element) elems.item(j);
                uuid = Long.parseLong(getElementValue(e));
                list.add(uuid);
            }

        }

        db.clearSponsors(list);
        if (callback != null)
            callback.callbackCall();
    }

    public void buildNotifications() {
        ArrayList<NewsEvent> list = db.getNewsNotify();

        int i = 0;
        for (NewsEvent event : list) {
            i++;
            notify(i, event.getTitle(), context.getString(R.string.app_name));
        }
    }

    public void notify(int mId, String title, String desc) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(title)
                        .setContentText(desc)
                        .setAutoCancel(true);

        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        mBuilder.setSound(alarmSound);
        long[] pattern = {500, 500, 500, 500};
        mBuilder.setVibrate(pattern);

        Intent resultIntent = new Intent(context, MainActivity.class);
        resultIntent.putExtra("fromNotification", true);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(MainActivity.class);

        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        mNotificationManager.notify(mId, mBuilder.build());
    }

    private boolean isOnline() {
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        boolean hasInternet = true;
        try {
            InetAddress ipAddr = InetAddress.getByName("google.pt");

            if (ipAddr.equals("")) {
                hasInternet = false;
            }

        } catch (Exception e) {
            hasInternet = false;
        }
        return (networkInfo != null && networkInfo.isConnected() && hasInternet);
    }

    public String getXmlFromUrl(URL url) {
        try {
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            return convertStreamToString(conn.getInputStream());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public String convertStreamToString(InputStream is) {

        if (is != null) {
            Writer writer = new StringWriter();

            char[] buffer = new char[1024];
            try {
                Reader reader = new BufferedReader(
                        new InputStreamReader(is, "UTF-8"));
                int n;
                while ((n = reader.read(buffer)) != -1) {
                    writer.write(buffer, 0, n);
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return writer.toString();
        } else {
            return "";
        }
    }

    public Document getDomElement(String xml) {
        Document doc;
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try {

            DocumentBuilder db = dbf.newDocumentBuilder();

            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(xml));
            doc = db.parse(is);

        } catch (ParserConfigurationException e) {
            return null;
        } catch (SAXException e) {
            return null;
        } catch (IOException e) {
            return null;
        }
        return doc;
    }

    public String getValue(Element item, String str) {
        NodeList n = item.getElementsByTagName(str);
        return this.getElementValue(n.item(0));
    }

    public final String getElementValue(Node elem) {
        Node child;
        if (elem != null) {
            if (elem.hasChildNodes()) {
                for (child = elem.getFirstChild(); child != null; child = child.getNextSibling()) {
                    if (child.getNodeType() == Node.TEXT_NODE) {
                        return child.getNodeValue();
                    }
                }
            }
        }
        return "";
    }

    private class DownloadMainXml extends AsyncTask<URL, Integer, String> {

        @Override
        protected String doInBackground(URL... urls) {
            return getXmlFromUrl(urls[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            saveMain(result);
        }

    }

    private class DownloadProgramXml extends AsyncTask<URL, Integer, String> {

        @Override
        protected String doInBackground(URL... urls) {
            return getXmlFromUrl(urls[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            saveProgram(result);
        }

    }

    private class DownloadNewsXml extends AsyncTask<URL, Integer, String> {

        @Override
        protected String doInBackground(URL... urls) {
            return getXmlFromUrl(urls[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            saveNews(result);
        }

    }

    private class DownloadSponsorsXml extends AsyncTask<URL, Integer, String> {

        @Override
        protected String doInBackground(URL... urls) {
            return getXmlFromUrl(urls[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            saveSponsors(result);
        }

    }
}
