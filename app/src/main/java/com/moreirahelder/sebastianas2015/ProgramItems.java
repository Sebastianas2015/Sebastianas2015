package com.moreirahelder.sebastianas2015;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by helder on 06-05-2015.
 */
public class ProgramItems {

    private boolean[] is_header;
    private ProgramEvent[] all_program;
    private List<Date> date;
    private List<List<ProgramEvent>> event;

    public ProgramItems(List<ProgramEvent> events) {

        date = new ArrayList<>();
        event = new ArrayList<>();
        if (events.size() == 0){
            return;
        }
        Date current_date = events.get(0).getDate();
        int n_headers = 0;
        event.add(new ArrayList<ProgramEvent>());
        event.get(n_headers).add(events.get(0));
        date.add(current_date);
        for (int i = 1; i < events.size(); i++) {
            date.add(null);
            if (!datesMatch(events.get(i).getDate(), current_date)){
                current_date = events.get(i).getDate();
                n_headers++;
                date.add(current_date);
                event.add(new ArrayList<ProgramEvent>());
            }
            event.get(n_headers).add(events.get(i));
        }
        int arrays_size = (n_headers+1)+events.size();
        is_header = new boolean[arrays_size];
        all_program = new ProgramEvent[arrays_size];
        buildIsHeader();
    }

    public void buildIsHeader(){
        int index = 0;
        for (List<ProgramEvent> day_program : event){
            is_header[index] = true;
            all_program[index] = null;
            index++;
            for (ProgramEvent event : day_program){
                is_header[index] = false;
                all_program[index] = event;
                index++;
            }
        }
    }

    public boolean isHeader(int index){
        return is_header[index];
    }

    public ProgramEvent get(int index){
        return all_program[index];
    }

    public Date getDate(int index){
        return date.get(index);
    }

    public int size(){
        if (all_program == null)
            return 0;
        return all_program.length;
    }

    public static boolean datesMatch(Date date1, Date date2) {
        if(date1.getDay() != date2.getDay())
            return false;
        if(date1.getMonth() != date2.getMonth())
            return false;
        if(date1.getYear() != date2.getYear())
            return false;
        return true;
    }
}
