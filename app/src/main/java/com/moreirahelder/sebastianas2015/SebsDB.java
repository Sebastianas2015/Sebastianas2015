package com.moreirahelder.sebastianas2015;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.content.Context;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.ListIterator;

/**
 * Created by helder on 30-04-2015.
 */
public class SebsDB extends SQLiteOpenHelper {

    private static SebsDB mInstance = null;

    public static final String DATABASE_NAME = "SebsDB.db";

    public static final String PROGRAM_TABLE_NAME = "program";
    public static final String PROGRAM_TITLE = "title";
    public static final String PROGRAM_DESCRIPTION = "description";
    public static final String PROGRAM_IMAGE = "image";
    public static final String PROGRAM_VIDEO = "video";
    public static final String PROGRAM_DATE = "event_date";

    public static final String NEWS_TABLE_NAME = "news";
    public static final String NEWS_TITLE = "title";
    public static final String NEWS_DESCRIPTION = "description";
    public static final String NEWS_IMAGE = "image";
    public static final String NEWS_VIDEO = "video";
    public static final String NEWS_DATE = "event_date";
    public static final String NEWS_NOTIFY = "notified";

    public static final String SPONSORS_TABLE_NAME = "sponsors";
    public static final String SPONSORS_NAME = "name";
    public static final String SPONSORS_IMAGE = "image";
    public static final String SPONSORS_LINK = "link";
    public static final String SPONSORS_ORDER = "p_order";

    private ImagesHandler imagesHandler;

    public static SebsDB getInstance(Context ctx) {
        if (mInstance == null) {
            mInstance = new SebsDB(ctx.getApplicationContext());
        }
        return mInstance;
    }

    private SebsDB(Context context) {
        super(context, DATABASE_NAME, null, 3);
        this.imagesHandler = ImagesHandler.getInstance(context);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + PROGRAM_TABLE_NAME + " (" +
                "id integer primary key not null," +
                PROGRAM_TITLE + " text," +
                PROGRAM_DESCRIPTION + " text," +
                PROGRAM_IMAGE + " text," +
                PROGRAM_VIDEO + " text," +
                PROGRAM_DATE + " integer)");

        db.execSQL("CREATE TABLE " + NEWS_TABLE_NAME + " (" +
                "id integer primary key not null," +
                NEWS_TITLE + " text," +
                NEWS_DESCRIPTION + " text," +
                NEWS_IMAGE + " text," +
                NEWS_VIDEO + " text," +
                NEWS_NOTIFY + " integer," +
                NEWS_DATE + " integer)");

        db.execSQL("CREATE TABLE " + SPONSORS_TABLE_NAME + " (" +
                "id integer primary key not null," +
                SPONSORS_NAME + " text," +
                SPONSORS_IMAGE + " text," +
                SPONSORS_LINK + " text," +
                SPONSORS_ORDER + " integer)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + PROGRAM_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + NEWS_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + SPONSORS_TABLE_NAME);
        onCreate(db);
    }

    public void reset() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS " + PROGRAM_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + NEWS_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + SPONSORS_TABLE_NAME);
        onCreate(db);
    }

    public boolean exists(String tablename, long uuid) {
        SQLiteDatabase db = this.getWritableDatabase();

        String Query = "Select * from " + tablename + " where id =?";
        Cursor cursor = db.rawQuery(Query, new String[]{"" + uuid});
        if (cursor.getCount() <= 0) {
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    public boolean delete(String tablename, long uuid) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(tablename, "id=?", new String[]{"" + uuid}) > 0;
    }

    public boolean insertProgramEvent(long uuid, String title, String desc, String image,
                                      String video, int day, int month, int hour, int minutes) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        boolean update = exists(PROGRAM_TABLE_NAME, uuid);

        Calendar current = Calendar.getInstance();
        GregorianCalendar calendar = new GregorianCalendar(current.get(Calendar.YEAR), month - 1, day, hour, minutes);

        if (!update) {
            contentValues.put("id", uuid);
        }

        contentValues.put(PROGRAM_TITLE, title);
        contentValues.put(PROGRAM_DESCRIPTION, desc);
        if (!image.equals(""))
            contentValues.put(PROGRAM_IMAGE, image);
        contentValues.put(PROGRAM_VIDEO, video);
        contentValues.put(PROGRAM_DATE, calendar.getTime().getTime());

        if (update) {
            db.update(PROGRAM_TABLE_NAME, contentValues, "id=?", new String[]{"" + uuid});
        } else {
            db.insert(PROGRAM_TABLE_NAME, null, contentValues);
        }

        return true;
    }

    public boolean ProgramSameImage(long uuid, String image) {
        SQLiteDatabase db = this.getReadableDatabase();
        String result = "";
        Cursor cursor = db.rawQuery("select " + PROGRAM_IMAGE + " from " + PROGRAM_TABLE_NAME + " where id == ?", new String[]{"" + uuid});
        if (cursor == null) {
            return false;
        }

        if (cursor.getCount() <= 0) {
            cursor.close();
            return false;
        }
        if (cursor.moveToFirst()) {
            result = cursor.getString(cursor.getColumnIndex(PROGRAM_IMAGE));
        }
        cursor.close();
        return result != null && result.equals(image);
    }

    public boolean insertNewsEvent(long uuid, String title, String desc, String image,
                                   String video, int day, int month, int hour, int minutes, int notify) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        boolean update = exists(NEWS_TABLE_NAME, uuid);

        Calendar current = Calendar.getInstance();
        GregorianCalendar calendar = new GregorianCalendar(current.get(Calendar.YEAR), month - 1, day, hour, minutes);

        if (!update) {
            contentValues.put("id", uuid);
        }
        contentValues.put(NEWS_TITLE, title);
        contentValues.put(NEWS_DESCRIPTION, desc);
        if (!image.equals(""))
            contentValues.put(NEWS_IMAGE, image);
        contentValues.put(NEWS_VIDEO, video);
        contentValues.put(NEWS_DATE, calendar.getTime().getTime());
        if (!update) {
            contentValues.put(NEWS_NOTIFY, notify);
        }

        if (update) {
            db.update(NEWS_TABLE_NAME, contentValues, "id=?", new String[]{"" + uuid});
        } else {
            db.insert(NEWS_TABLE_NAME, null, contentValues);
        }

        return true;
    }

    public boolean NewsSameImage(long uuid, String image) {
        SQLiteDatabase db = this.getReadableDatabase();
        String result = "";
        Cursor cursor = db.rawQuery("select " + NEWS_IMAGE + " from " + NEWS_TABLE_NAME + " where id == ?", new String[]{"" + uuid});
        if (cursor == null)
            return false;
        if (cursor.getCount() <= 0) {
            cursor.close();
            return false;
        }
        if (cursor.moveToFirst()) {
            result = cursor.getString(cursor.getColumnIndex(NEWS_IMAGE));
        }
        cursor.close();
        return result != null && result.equals(image);
    }

    public boolean insertSponsor(long uuid, String title, String image, String link, int order) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        boolean update = exists(SPONSORS_TABLE_NAME, uuid);

        if (!update) {
            contentValues.put("id", uuid);
        }
        contentValues.put(SPONSORS_NAME, title);
        if (!image.equals(""))
            contentValues.put(SPONSORS_IMAGE, image);
        contentValues.put(SPONSORS_LINK, link);
        contentValues.put(SPONSORS_ORDER, order);

        if (update) {
            db.update(SPONSORS_TABLE_NAME, contentValues, "id=?", new String[]{"" + uuid});
        } else {
            db.insert(SPONSORS_TABLE_NAME, null, contentValues);
        }
        return true;
    }

    public boolean SponsorSameImage(long uuid, String image) {
        SQLiteDatabase db = this.getReadableDatabase();
        String result = "";
        Cursor cursor = db.rawQuery("select " + SPONSORS_IMAGE + " from " + SPONSORS_TABLE_NAME + " where id == ?", new String[]{"" + uuid});
        if (cursor == null)
            return false;
        if (cursor.getCount() <= 0) {
            cursor.close();
            return false;
        }
        if (cursor.moveToFirst()) {
            result = cursor.getString(cursor.getColumnIndex(SPONSORS_IMAGE));
        }
        cursor.close();
        return result != null && result.equals(image);
    }

    public void clearProgram(ArrayList<Long> list) {
        for (Long uuid : list) {
            delete(PROGRAM_TABLE_NAME, uuid);
            imagesHandler.deleteImage(uuid);
        }
    }

    public void clearNews(ArrayList<Long> list) {
        for (Long uuid : list) {
            delete(NEWS_TABLE_NAME, uuid);
            imagesHandler.deleteImage(uuid);
        }
    }

    public void clearSponsors(ArrayList<Long> list) {
        for (Long uuid : list) {
            delete(SPONSORS_TABLE_NAME, uuid);
            imagesHandler.deleteImage(uuid);
        }
    }

    public ArrayList<ProgramEvent> getProgram() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from " + PROGRAM_TABLE_NAME, null);
        ArrayList<ProgramEvent> list = new ArrayList<>();
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                ProgramEvent item = new ProgramEvent();
                item.setUuid(Long.parseLong(cursor.getString(cursor.getColumnIndex("id"))));
                item.setTitle(cursor.getString(cursor.getColumnIndex(PROGRAM_TITLE)));
                item.setDesc(cursor.getString(cursor.getColumnIndex(PROGRAM_DESCRIPTION)));
                item.setImage(cursor.getString(cursor.getColumnIndex(PROGRAM_IMAGE)));
                item.setVideo(cursor.getString(cursor.getColumnIndex(PROGRAM_VIDEO)));
                item.setDate(new Date(cursor.getLong(cursor.getColumnIndex(PROGRAM_DATE))));
                list.add(item);
                cursor.moveToNext();
            }
        }
        cursor.close();
        Collections.sort(list);
        return list;
    }

    public ArrayList<NewsEvent> getNews() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from " + NEWS_TABLE_NAME + " where ? >= " + NEWS_DATE, new String[]{"" + new Date().getTime()});
        ArrayList<NewsEvent> list = new ArrayList<>();
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                NewsEvent item = new NewsEvent();
                item.setUuid(Long.parseLong(cursor.getString(cursor.getColumnIndex("id"))));
                item.setTitle(cursor.getString(cursor.getColumnIndex(NEWS_TITLE)));
                item.setDesc(cursor.getString(cursor.getColumnIndex(NEWS_DESCRIPTION)));
                item.setImage(cursor.getString(cursor.getColumnIndex(NEWS_IMAGE)));
                item.setVideo(cursor.getString(cursor.getColumnIndex(NEWS_VIDEO)));
                item.setDate(new Date(cursor.getLong(cursor.getColumnIndex(NEWS_DATE))));
                item.setNotify(cursor.getInt(cursor.getColumnIndex(NEWS_NOTIFY)));
                list.add(item);
                cursor.moveToNext();
            }
        }
        cursor.close();
        Collections.sort(list);
        Collections.reverse(list);
        ListIterator li = list.listIterator(list.size());
        while (li.hasPrevious()) {
            NewsEvent item = (NewsEvent) li.previous();
            item.setBitmap(imagesHandler.getImage(item.getUuid()));
        }
        return list;
    }

    public ArrayList<Sponsor> getSponsors() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from " + SPONSORS_TABLE_NAME, null);
        ArrayList<Sponsor> list = new ArrayList<>();
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                Sponsor item = new Sponsor();
                item.setUuid(Long.parseLong(cursor.getString(cursor.getColumnIndex("id"))));
                item.setName(cursor.getString(cursor.getColumnIndex(SPONSORS_NAME)));
                item.setLink(cursor.getString(cursor.getColumnIndex(SPONSORS_LINK)));
                item.setImage(cursor.getString(cursor.getColumnIndex(SPONSORS_IMAGE)));
                item.setOrder(Integer.parseInt(cursor.getString(cursor.getColumnIndex(SPONSORS_ORDER))));
                item.setBitmap(imagesHandler.getImage(item.getUuid()));
                list.add(item);
                cursor.moveToNext();
            }
        }
        cursor.close();
        Collections.sort(list);
        ListIterator li = list.listIterator(list.size());
        while (li.hasPrevious()) {
            Sponsor item = (Sponsor) li.previous();
            item.setBitmap(imagesHandler.getImage(item.getUuid()));
        }
        return list;
    }

    public ArrayList<NewsEvent> getNewsNotify() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from " + NEWS_TABLE_NAME + " where ? >= " + NEWS_DATE + "  and " + NEWS_NOTIFY + " == 1",
                new String[]{"" + new Date().getTime()});
        ArrayList<NewsEvent> list = new ArrayList<>();
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                NewsEvent item = new NewsEvent();
                item.setUuid(cursor.getLong(cursor.getColumnIndex("id")));
                item.setTitle(cursor.getString(cursor.getColumnIndex(NEWS_TITLE)));
                item.setDesc(cursor.getString(cursor.getColumnIndex(NEWS_DESCRIPTION)));
                item.setImage(cursor.getString(cursor.getColumnIndex(NEWS_IMAGE)));
                item.setVideo(cursor.getString(cursor.getColumnIndex(NEWS_VIDEO)));
                item.setDate(new Date(cursor.getLong(cursor.getColumnIndex(NEWS_DATE))));
                item.setNotify(cursor.getInt(cursor.getColumnIndex(NEWS_NOTIFY)));
                list.add(item);
                cursor.moveToNext();
            }
            cursor.close();
        }
        for (NewsEvent event : list) {
            setNotified(event.getUuid());
        }
        return list;
    }

    public boolean setNotified(long uuid) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(NEWS_NOTIFY, 0);
        return db.update(NEWS_TABLE_NAME, contentValues, "id=?", new String[]{"" + uuid}) > 0;
    }
}
