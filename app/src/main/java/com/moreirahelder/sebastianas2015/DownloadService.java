package com.moreirahelder.sebastianas2015;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.preference.PreferenceManager;

public class DownloadService extends Service {

    public static final int MINUTE = 60 * 1000;
    int update_interval;
    boolean notify;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        update_interval = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(this).getString("update_interval", "60"));
        notify = PreferenceManager.getDefaultSharedPreferences(this).getBoolean("notifications", true);
        new ContentDownload().execute();
        stopSelf();
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        AlarmManager alarm = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarm.set(
                AlarmManager.RTC_WAKEUP,
                System.currentTimeMillis() + (MINUTE * update_interval),
                PendingIntent.getService(this, 0, new Intent(this, DownloadService.class), 0)
        );
    }

    private class ContentDownload extends AsyncTask {

        public ContentDownload() {

        }

        @Override
        protected Object doInBackground(Object[] params) {
            DownloadContents download = new DownloadContents(getApplicationContext(), true, null);
            download.updateMainSettings();
            download.updateProgram();
            download.updateNews();
            download.updateSponsors();
            if (notify) {
                download.buildNotifications();
            }
            return null;
        }
    }
}
