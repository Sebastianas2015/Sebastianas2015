# Sebastianas 2015 - Android

Android APP for the Sebastianas festival in Freamunde, Portugal. Main features:

  - Show festival's program
  - Show festival's map
  - Notify and show other events and news
  - Provide information on how to get to the festival
  - Provide livestreams during the festival
  - Show important contacts and addresses

__Note 1:__ Due to financial constraints it was not possible to use a server for pushing notifications, and so they are made on device side, by periodically download of XML static files containing the information.

__Note 2:__ It is possible to find many bugs and bad code on this software due to time restrictions. Both iOS and Android APPs, along with the simple dashboard were developed in just over one month.

### Version
3.1.0

License
----

Apache License V2
